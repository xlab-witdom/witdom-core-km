#!/bin/bash -e

#ctx logger info "Started executing Barbican startup script"

#source /home/$USER/.barbicanenv/bin/activate
#ctx logger info "Activated virtualenv"

#cd /home/$USER/barbican/bin

# Still blocks execution from stopping properly, but works
#sudo nohup ./barbican.sh start > barbican.out 2> barbican.err < /dev/null &
#nohup ./barbican.sh start > /dev/null 2>&1 &
#ctx logger info "Starting barbican service as a daemon"
#nohup source barbican.sh start > /dev/null 2>&1 &

ctx logger info "Barbican up and running, deployment DONE."