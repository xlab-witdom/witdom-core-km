name 'barbican'
description 'Cookbook for setting up OpenStack Barbican API'
version '1.0.0'

maintainer 'Manca Bizjak'
maintainer_email 'manca.bizjak@xlab.si'

depends 'poise-python'
