home = "/home/#{node['barbican']['user']}"

execute 'Updating packages' do
	command 'sudo apt-get update'
	ignore_failure true
end

# Dependencies to be installed prior to Barbican setup 
package ["git",
		"python-dev",
		"python-pip",
		"python-virtualenv",
		"libsqlite3-dev",
		"libxml2-dev",
		"libsasl2-dev",
		"libxslt1-dev",
		"libldap2-dev",
		"libffi-dev",
		"libssl-dev"]
	
# Location where Barbican will be installed
barbican_dir = "#{home}/barbican"

bash "Creating virualenv, installing some packages with pip" do
  cwd "#{home}"
  code <<-EOH
	  virtualenv .barbicanenv
	  source .barbicanenv/bin/activate
	  pip install pecan alembic python-ldap pytz
  EOH
end

# Clone barbican repository
git barbican_dir do
	repository "https://github.com/openstack/barbican.git"
	action :checkout
    revision "e3ac69a0086dfabe612a88ee7b7afce631ec8e3d" # This is a commit from july...
end

# Install barbican
bash "Starting virtual environment and installing barbican" do
  cwd barbican_dir
  code <<-EOH
	  source #{home}/.barbicanenv/bin/activate
	  pip install -e $PWD
  EOH
end

# Copy template with barbican configuration to this node
template "#{barbican_dir}/etc/barbican/barbican-api-paste.ini" do 
	source 'barbican-api-paste.erb'
	mode '0755'
	variables({
		:keystone_ip => node['barbican']['auth']['keystone_ip'],
	})
end

# Starting barbican
bash "Starting barbican from virtualenv" do
  cwd barbican_dir
  code <<-EOH
	  source #{home}/.barbicanenv/bin/activate
	  cd bin
      nohup source barbican.sh start > barbican.out 2>&1 &
  EOH
end
