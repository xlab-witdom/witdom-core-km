# WITDOM KM - Barbican
This document describes basic setup and usage of OpenStack Key Management (KM) service Barbican, which is started inside a docker conrainer and can be used for local development.

### Files included
* _Dockerfile_ - used to build the docker image for unauthenticated Barbican
* _Dockerfile_auth_ - used to build docker image for Barbican that uses Keystone authentication token middleware
* _start.sh_ - a script that runs Barbican from within the Docker container
* _Makefile_ - can be optionally used for faster building, if preferred

>**Note**: This guide assumes that Barbican is used in an unauthenticated context.

### Prerequisites
* Docker
* optional: REST Client like curl, Postman or ARS (for initial testing), GNU make

##  Instructions for building Docker image and running it in a container
### Option 1 - building manually, without Make
1. Navigate to the root directory of this project (it contains Dockerfile) and make sure that script _start.sh_ has execute permissions

    ````bash
    $ chmod a+x start.sh
    ````

2. To **build docker image** named _km_ run: 

    ````bash
    $ docker build -t km .
    ````
    This step will take a while to complete.

3. To **create and run the docker container** named _km_ with previously created image, run:
 
    ````
    $ docker run --name km -p 9311:9311 -t km
    ````
    With the above command we also forward Barbican's public port (9311) to our local machine. Thus, Barbican service running inside docker container will be accessible from localhost.

4. **Verify that Barbican KM service is running correctly**

    After running the `docker run` command from the previous step Barbican should be running in the container and you should see its output, similar to what is shown below: 
    
    ````bash
    *** Operational MODE: single process ***
    *** uWSGI is running in multiple interpreter mode ***
    spawned uWSGI master process (pid: 15)
    Fri Oct 28 07:12:37 2016 - [emperor] vassal barbican-api.ini has been spawned
    spawned uWSGI worker 1 (pid: 16, cores: 1)
    Loading paste environment: config:/etc/barbican/barbican-api-paste.ini
    2016-10-28 07:12:39.056 16 INFO barbican.model.repositories [-] Setting up database engine and session factory
    2016-10-28 07:12:39.072 16 INFO barbican.model.repositories [-] Auto-creating barbican registry DB
    2016-10-28 07:12:40.096 16 WARNING barbican.model.migration.commands [-] !!! Limited support for migration commands using sqlite databases; This operation may not succeed.
    2016-10-28 07:12:40.107 16 INFO alembic.runtime.migration [-] Context impl SQLiteImpl.
    2016-10-28 07:12:40.107 16 INFO alembic.runtime.migration [-] Will assume non-transactional DDL.
    2016-10-28 07:12:40.168 16 INFO alembic.runtime.migration [-] Running stamp_revision  -> 39cf2e645cba
    2016-10-28 07:12:40.541 16 INFO barbican.api.app [-] Barbican app created and initialized
    WSGI app 0 (mountpoint='') ready in 3 seconds on interpreter 0x26f0650 pid: 16
    Fri Oct 28 07:12:40 2016 - [emperor] vassal barbican-api.ini is ready to accept requests
    ````
    
    A quick check to see if everything is working properly, is by visiting _http://127.0.0.1:9311_ in your browser. You should see some metadata.

    #### Accessing container's filesystem
    If at any point you need access to filesystem inside docker container (for instance, to see logs of background processes), run the following command in another terminal window
       
    ````bash
    $ sudo docker exec -it km bash
    ````

    #### Stopping & removing docker container

    ````bash
    $ sudo docker stop km && sudo docker rm km
    ````

    Note that removing a container will result in the loss of the data inside container. If you don't want to lose the data, just start the container again (see below).

    #### Restarting container
    You can restart stopped container by running

    ````bash
    $ sudo docker start km
    ````

### Option 2 - building with Make
* To build the image and run the KM container, simply run `make`
* To stop and remove the container run `make clean`
* To log-in to the container run `make bash`


## Some information regarding dockerized Barbican
As this Docker image is meant purely for ease of local development of WITDOM components that depend on KM, Barbican is **not used in an authenticated context**. This means that Barbican KM service is not integrated with Keystone IAM service to use authentication tokens.

From [Barbican developer documentation](http://docs.openstack.org/developer/barbican/setup/noauth.html):

> With every OpenStack service integrated with keystone, its API requires access token to retireve certain information and validate user’s information and prviledges. **If you are running barbican in no auth mode, you have to specify _project_id_ instead of an _access token_ which was retrieved from the token instead. In case of API, replace 'X-Auth-Token: $TOKEN' with 'X-Project-Id: {project_id}' for every API request in Barbican API Documentation**

Barbican's API can thus be used wihout providing authentication tokens, you just specify the _X-Project-Id_ header with an arbitrary value. In a production setting (where Keystone authentication tokens are used) however, Barbican's API is to be used in **exactly the same way, but with a different header (_X-Auth-Token_) specifying an authentication token instead**.


# Examples
* Here is an example of a request to unauthenticated Barbican:
    
    ````
    POST http://localhost:9311/v1/secrets
    Content-Type: application/json
    X-Project-Id: my-project-id

    {
        "payload": "MY_SECRET",
        "payload_content_type": "text/plain"
    }
    ````

    If everything is working properly, you should get a reply like this:

    ````
    {
        "secret_ref": "http://localhost:9311/v1/secrets/ef02f82d-113d-4649-8fc1-86c881f2f720"
    }
    ````

* For Python examples for interacting with Barbican please refer to [barbican python examples](https://bitbucket.org/xlab-witdom/witdom-core-km/src/master/integration-tests/python). However, these examples assume Barbican in an authenticated context and use a Keystone instance for retrieving authentication tokens.
* For HTTP request examples on how to use Barbican API, please refer to [Barbican API Reference](http://docs.openstack.org/developer/barbican/api/index.html). Prior to that, it might be helpful to read [Barbican API Guide](http://developer.openstack.org/api-guide/key-manager/). There you can find more detailed description of Barbican terminology and what API endpoints are most appropriate for your WITDOM component's requirements.