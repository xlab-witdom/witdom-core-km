#!/bin/bash

cd /barbican
# Start barbican from virtualenv
source .barbicanenv/bin/activate
./bin/barbican.sh start &

barbican secret store --os-project-id test --name swiftuser --payload test:tester --no-auth --endpoint http://localhost:9311
barbican secret store --os-project-id test --name swiftpw --payload testing --no-auth --endpoint http://localhost:9311
barbican secret store --os-project-id test --name swift --payload swiftpw --no-auth --endpoint http://localhost:9311
barbican secret store --os-project-id test --name mysql --payload mysqlpw --no-auth --endpoint http://localhost:9311
barbican secret store --os-project-id test --name oracle --payload oraclepw --no-auth --endpoint http://localhost:9311

# For MySQL
barbican secret store --os-project-id test --name mysql-user --payload witdomuser --no-auth --endpoint http://localhost:9311
barbican secret	store --os-project-id test --name mysql-pass --payload witdomuserpw --no-auth --endpoint http://localhost:9311
barbican secret store --os-project-id test --name witdomuser --payload witdomuserpw --no-auth --endpoint http://localhost:9311

./bin/barbican.sh restart
