FROM ubuntu:14.04
LABEL Description="This image starts OpenStack KM Service Barbican."

RUN apt-get update && apt-get install -y \
	build-essential \
	curl \
	git \
	libffi-dev \
	libpcre3 \
	libpcre3-dev \
	libreadline-dev \
	libssl-dev \
	libsqlite3-dev \
	libxml2-dev \
	libxslt1-dev \
	libbz2-dev \
	python-dev \
	python-pip

##################################
# BARBICAN SETUP AND CONFIGURATION
##################################
# Clone Barbican repository from Git
RUN git clone https://github.com/openstack/barbican.git
RUN pip install virtualenv

WORKDIR barbican
RUN git checkout tags/3.0.0

# Create and activate virtualenvironment, install dependencies in it
RUN virtualenv .barbicanenv  && \
	. .barbicanenv/bin/activate && \
	pip install -e $PWD && \
	pip install -U pastedeploy && \
	pip install python-barbicanclient uwsgi

# Create appropriate directories, copy configuration
RUN	 mkdir /etc/barbican && \
	 mkdir /var/lib/barbican && \
	 cp -r etc/barbican /etc

# Paste config file with settings to use authenticated version of Barbican (with Keystone authtokens),
COPY ./conf/barbican-api-paste.ini /etc/barbican/barbican-api-paste.ini

# Copy startup script
COPY ./start.sh /

WORKDIR /

CMD ./start.sh

# Expose Barbican public endpoint
EXPOSE 9311