import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import core.km.client.components.Container;
import core.km.client.components.Order;
import core.km.client.components.Secret;
import core.km.client.main.KMClient;
import core.km.client.utils.KMException;

public class RunClient {

	public static void main(String[] args) {
		if (args.length != 3) {
			System.out.println("Correct usage is RunClient BarbicanURL AuthToken, ProjecId" +args.length );
			return;
		}
		try {
			KMClient km = new KMClient(new URL(args[0]), args[1], args[2]);
			testSecrets(km);
			testContainers(km);
			testMetadata(km);
			testOrders(km);
			
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (KMException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static void testSecrets(KMClient kMClient) throws KMException {
		// Store secret
		kMClient.secrets().store("emptySecret", null, null);
		System.out.println("Secret stored.");
		
		// List secrets
		List<Secret> secrets = kMClient.secrets().list(null, null, "");
		System.out.println("Secrets listed: " + secrets.size());
		
		// Get secret
		Secret secret = kMClient.secrets().get("emptySecret");
		System.out.println("Secret obtained: " + secret.getName());
		
		// Store payload
		kMClient.secrets().storePayload("emptySecret", "This is the added payload", "text/plain", null);
		System.out.println("Payload stored.");
		
		// Get payload
		String payload = kMClient.secrets().getPayload(secret.getName());
		System.out.println("Secret: " + secret.getName() + " | Payload: " + payload);
		
		// Delete secret
		kMClient.secrets().delete(secret.getName());
		System.out.println("Secret deleted.");
	}
	
	public static void testContainers(KMClient kMClient) throws KMException {
		// Create a container
		kMClient.containers().create("bat-container", "generic", null);
		System.out.println("Container created.");
		
		// List containers
		List<Container> containers = kMClient.containers().list(null, null);
		System.out.println("Containers listed: " + containers.size());
		
		// Get container
		Container container = kMClient.containers().get("bat-container");
		System.out.println("Container obtained: " + container.getName());
		
		kMClient.secrets().store("aboutBatman", "Batman is Bruce Wayne", "text/plain");
		Secret secret = kMClient.secrets().get("aboutBatman");
		// Add secret to container
		kMClient.containers().addSecret(container.getName(), "batmanSecret", secret.getRef());
		System.out.println("Added secret [" + secret.getName() + "] to container [" + container.getName() + "]");
		
		// Delete container
		kMClient.containers().delete(container.getName());
		System.out.println("Container deleted.");
	}
	
	public static void testMetadata(KMClient kMClient) throws KMException {
		Map<String, String> metadata = new HashMap<String,String>();
		metadata.put("name", "alfred");
		metadata.put("job", "butler");
		
		// Store metadata
		kMClient.secrets().metadata().store("aboutBatman", metadata);
		System.out.println("Metadata stored.");
		
		// List metadata
		Map<String, String> metadataReceived = kMClient.secrets().metadata().list("aboutBatman");
		System.out.println("Metadata listed: " + metadataReceived.size());
		
		// Get metadata value
		String metadataValue = kMClient.secrets().metadata().get("aboutBatman", "name");
		System.out.println("Metadata obtained [name]: " + metadataValue);
		
		// Add metadata
		kMClient.secrets().metadata().add("aboutBatman", "added-key", "added-value");
		System.out.println("Metadata added.");
		
		// Update metadata
		kMClient.secrets().metadata().update("aboutBatman", "added-key", "updated-value");
		System.out.println("Metadata updated.");
		
		// Delete metadata
		kMClient.secrets().metadata().delete("aboutBatman", "job");
		System.out.println("Metadata deleted.");
	}
	
	public static void testOrders(KMClient kMClient) throws KMException {
		Map<String, Object> metadata = new HashMap<String, Object>();
		metadata.put("name", "secretName");
		metadata.put("algorithm", "AES");
		metadata.put("bit_length", 256);
		
		// Create order
		kMClient.orders().create("key", metadata);
		System.out.println("Order created.");
		
		// List orders
		List<Order> orders = kMClient.orders().list(null, null);
		System.out.println("Orders listed: " + orders.size());
		
		// Get order
		//Order order = kMClient.orders().get("testOrder"); // Not developed yet
		
		// Delete order
		//kMClient.orders().delete("testOrder"); // Not developed yet
	}
}
