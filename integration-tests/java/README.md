# Example: Using barbican services

This example shows how to use Java Client in order to generate, store or access keys in Barbican

Assuming you have installed Java project according to installation instructions in barbican-clients/java/, then `mvn install` command created a .jar inside your local maven repository (~.m2\repository\com\witdom\witdom-core-km-client\0.0.1-SNAPSHOT\witdom-core-km-client-0.0.1-SNAPSHOT) with ending *-jar-with-dependencies.jar*.
To use Java Client in Java code (it does not have to be built with maven), you must add this jar to Java classpath. 

One way to do this is to edit your CLASSPATH environment variable. Another is to add it to Java classpath when compiling and running code like this:

* Compilation:
    ````
    javac -cp "/path/to/your/local/jar-with-dependencies.jar:." RunClient.java
    ````
    
* Running the code:
Three parameters are required to use the barbican client with an online service:
 * The URL of the Barbican endpoint
 * A valid user token. See [HERE] (https://bitbucket.org/xlab-witdom/witdom-core-iam/src/a904e44c481657aa23e1ef0ce8393d755778df6a/keystone-clients/integration-tests/java/README.md) how to obtain it 
 * A projectId 
 
    ````
    java  -cp "/path/to/your/local/jar-with-dependencies.jar:." RunClient EndpointURL X-AUTH-TOKEN PROJECTID
    # Runs client example, which uses the different servicesand prints it to console
    ````