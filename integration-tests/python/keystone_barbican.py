import argparse
from keystoneauth1.identity import v3
from keystoneauth1 import session
from keystoneclient.v3 import client
from barbicanclient import client as barbicanclient

# Define and parse command line arguments
parser = argparse.ArgumentParser(description='Simple usage examples of Keystone and Barbican Python client')
parser.add_argument('ip_keystone', nargs='?', type=str, help="IP address where Keystone service is accessible", default="localhost")
parser.add_argument('ip_barbican', nargs='?', type=str, help="IP address where Barbican service is accessible", default="localhost")

args = parser.parse_args()
ip_keystone = args.ip_keystone
ip_barbican = args.ip_barbican

# Define endpoints for Keystone and Barbican services
keystone_endpoint = 'http://%s:5000/v3' % (ip_keystone)
barbican_endpoint = 'http://%s:9311' %(ip_barbican)

##### PART 1 ##### : Authenticate with Keystone in order to retrieve authentication token
# Authenticate with u+pw credentials to retrieve a token
auth = v3.Password(auth_url=keystone_endpoint, username='admin', password='adminpw', user_domain_id='default')
sess = session.Session(auth=auth)
keystone = client.Client(session=sess)

# Retrieve authentication info that includes token
access_info = keystone.get_raw_token_from_identity_service(keystone_endpoint, username='admin', password='adminpw' , user_domain_id="default")

# Extract authentication token 
auth_token = access_info.auth_token

# Retrieve id of service project (used by barbican)
service_project = None
for project in keystone.projects.list():
	if project.name == "service":
		service_project = project
		break

print "Service project id: {}".format(service_project.id)

##### PART 2 ##### : Interact with Barbican by using previously obtained token from Keystone
token = v3.TokenMethod(token=auth_token) # Get obtained token into appropriate structure
token_auth = v3.Auth(auth_url=keystone_endpoint, auth_methods=[token]) 
barbican_session = session.Session(auth=token_auth)
barbican = barbicanclient.Client(session=barbican_session, endpoint=barbican_endpoint, project_id=service_project.id)

# Create a secret
secret = barbican.secrets.create(name="my_secret", payload="This is a test secret to see if everything works fine")
print "Locally created secret\n {}".format(secret)

# Store a secret with Barbican
secret_ref = secret.store()
secret_ref = secret_ref.replace("localhost", ip_barbican) # Necessary if Barbican is not running on localhost
print "Successfully stored secret secret to Barbican, it's reference: {}".format(secret_ref)

# Retrieve a secret from Barbican
retrieved_secret = barbican.secrets.get(secret_ref)
secret_data = retrieved_secret.payload
print "You successfully retrieved your secret: {}".format(secret_data)