# Examples for interacting with Barbican from Python
This document describes prerequisites, instructions and additional information for running simple Barbican client examples in Python.

## Prerequisites
* A running Keystone instance (for instance, inside a Docker container)
* A running Barbican instance (for instance, inside a Docker container)
* Python packages _python-keystoneclient_, _python-barbicanclient_ and _keystoneauth1_ on your local machine. You can install them with pip:

	````bash
	$ pip install python-keystoneclient python-barbicanclient keystoneauth1
	```` 

## keystone-barbican.py
A very simple example showing how to use Keystone authentication token for accessing Barbican API. A user retrieves authentication token from Keystone and sends it with subsequent requests to Barbican in order to generate and retreive his secret payload.

_Note: The Keystone user interacting with Barbican must have appropriate privileges assigned, otherwise Barbican API calls will result in "Forbidden" HTTP responses._

````bash
$ python keystone-barbican.py -h  # Shows help
$ python keystone-barbican.py     # Runs an example, assumes Barbican and Keystone are both accessible on localhost
```` 
