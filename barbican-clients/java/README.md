# Java Client for accessing Barbican services
This project wraps code for interacting with OpenStack Barbican for generating, storing or accessing keys in an easy way.

## Prerequisites
* A Keystone instance (for instance, inside a Docker container)
* A Barbican instance (for instance, inside a Docker container)
* Apache Maven for building project

## Installation
*Note: prior to installation, ensure that maven project OpenStack Java SDK (see prerequisites) is installed.*
1. Clone this repository to an arbitrary location
2. `cd` to the project's root directory (where _pom.xml_ is located)
3. Run `mvn install` which will build this project
4. Run `mvn test` to compile and run tests with a mocked Barbican component

## Modifying source code
- If you wish to modify these source files, run `mvn compile` from the root directory 
- If you modify testing code, just run `mvn test` to compile and run unit tests

## Using Java Client in your code
Please see the example [here](https://bitbucket.org/xlab-witdom/witdom-core-km/src/master/integration-tests/java/README.md)