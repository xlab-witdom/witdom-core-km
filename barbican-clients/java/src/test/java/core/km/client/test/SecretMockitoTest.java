package core.km.client.test;

import java.io.ByteArrayInputStream;
import java.net.URL;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import core.km.client.main.KMClient;
import core.km.client.services.SecretService;
import junit.framework.TestCase;

@RunWith(MockitoJUnitRunner.class)
public class SecretMockitoTest extends TestCase {
	private String test2SecretJsonString = "{    \"next\": \"http://{barbican_host}:9311/v1/secrets?limit=2&offset=3\",    \"previous\": \"http://{barbican_host}:9311/v1/secrets?limit=2&offset=0\",    \"secrets\": [        {            \"algorithm\": null,            \"bit_length\": null,            \"content_types\": {                \"default\": \"application/octet-stream\"            },            \"created\": \"2015-04-07T03:37:19.805835\",            \"creator_id\": \"3a7e3d2421384f56a8fb6cf082a8efab\",            \"expiration\": null,            \"mode\": null,            \"name\": \"opaque octet-stream base64\",            \"secret_ref\": \"http://{barbican_host}:9311/v1/secrets/{uuid}\",            \"secret_type\": \"opaque\",            \"status\": \"ACTIVE\",            \"updated\": \"2015-04-07T03:37:19.808337\"        },        {            \"algorithm\": null,            \"bit_length\": null,            \"content_types\": {                \"default\": \"application/octet-stream\"            },            \"created\": \"2015-04-07T03:41:02.184159\",            \"creator_id\": \"3a7e3d2421384f56a8fb6cf082a8efab\",            \"expiration\": null,            \"mode\": null,            \"name\": \"opaque random octet-stream base64\",            \"secret_ref\": \"http://{barbican_host}:9311/v1/secrets/{uuid}\",            \"secret_type\": \"opaque\",            \"status\": \"ACTIVE\",            \"updated\": \"2015-04-07T03:41:02.187823\"        }    ],    \"total\": 2}";
	HttpClient mockClient = Mockito.mock(HttpClient.class);
	HttpResponse mockResponse = Mockito.mock(HttpResponse.class);
	StatusLine mockStatusLine = Mockito.mock(StatusLine.class);
	HttpEntity mockHttpEntity= Mockito.mock(HttpEntity.class);
	String url = "http://localhost";
	String xAuthToken = "1231231232";
	SecretService instance = null;
	
	@Before public void intializeMocks() {
		try {
			instance = Mockito.spy(new KMClient(new URL(url), xAuthToken, "").secrets());
			
			Mockito.when(instance.getHttpClient()).thenReturn(mockClient);
			Mockito.when(mockClient.execute(Mockito.isA(HttpGet.class))).thenReturn(mockResponse);
		    Mockito.when(mockResponse.getStatusLine()).thenReturn(mockStatusLine);
		    Mockito.when(mockStatusLine.getStatusCode()).thenReturn(HttpStatus.SC_OK);
		    Mockito.when(mockResponse.getEntity()).thenReturn(mockHttpEntity);
		    Mockito.when(mockHttpEntity.getContent()).thenReturn(new ByteArrayInputStream(test2SecretJsonString.getBytes()));		
		} catch (Exception e) {
			fail("Exception captured");
		}
	}
	
	@Test
	public void test1() {
		/*int offset = 0;
		int count = 10;
		String name = "name";
		try {
			
			ArgumentCaptor<HttpGet> requestCaptor = ArgumentCaptor.forClass(HttpGet.class);
		    
			// Test that the test2SecretJsonString is succesfully parsed to 2 secrets
		    assertEquals(2,instance.list(offset, count, name).size());
		    
		    
		    // Test that a HTTPGet was executed
		    Mockito.verify(mockClient).execute(requestCaptor.capture());
		    // Test URL
		    assertEquals(url + "/secrets/?offset=" + offset + "&limit=" + count +"&name="+name , requestCaptor.getValue().getURI().toString());
		    // Test token header
		    assertEquals(xAuthToken, requestCaptor.getValue().getHeaders("X-Auth-Token")[0].getValue());
		    // Test content type 
		    assertEquals("application/json", requestCaptor.getValue().getHeaders("content-type")[0].getValue());
		    
		} catch (Exception e) {
			fail("Exception captured");
		}
	*/	
	}
}
