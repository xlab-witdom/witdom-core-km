package core.km.client.test;

public class TestEnvironment {
	
	private static final String TEST_INTEGRATION_WITH_SERVER = "testIntegrationWithServer";

	public static boolean testIntegrationWithServer() {
		return "true".equalsIgnoreCase(System.getProperty(TEST_INTEGRATION_WITH_SERVER));
	}
}
