package core.km.client.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public final class ResponseUtils {
	
	public static String bodyToString(InputStream contentInputStream) throws IOException {
		
		BufferedReader bufferedReader = null;
		StringBuilder stringBuilder = new StringBuilder();

		String line;
		try {
			bufferedReader = new BufferedReader(new InputStreamReader(contentInputStream));
			while ((line = bufferedReader.readLine()) != null) {
				stringBuilder.append(line);
			}
		} catch (IOException e) {
			throw e;
		} finally {
			if (bufferedReader != null) {
				try {
					bufferedReader.close();
				} catch (IOException e) {
					throw e;
				}
			}
		}

		return stringBuilder.toString();
	}
}
