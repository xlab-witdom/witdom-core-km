package core.km.client.components;

import java.util.Map;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
public class Secret {
	
	@JsonProperty("secret_ref")
	protected String ref;
	@JsonProperty("secret_type")
	protected String type;
	@JsonProperty("name")
	protected String name;
	@JsonProperty("created")
	protected String created;
	@JsonProperty("updated")
	protected String updated;
	@JsonProperty("status")
	protected String status;
	@JsonProperty("expiration")
	protected String expiration;
	@JsonProperty("algorithm")
	protected String algorithm;
	@JsonProperty("bit_length")
	protected Integer bitLength;
	@JsonProperty("mode")
	protected String mode;
	@JsonProperty("payload")
	protected String payload;
	@JsonProperty("payload_content_type")
	protected String payloadContentType;
	@JsonProperty("payload_content_encoding")
	protected String payloadContentEncoding;
	@JsonProperty("content_types")
	protected Map<String, String> contentTypes;
	@JsonProperty("creator_id")
	protected String creatorId;
	
	public String getRef() {
		return ref;
	}
	
	public String getName() {
		return name;
	}
	
	public String getType() {
		return type;
	}
	
	public String getCreated() {
		return created;
	}
	
	public String getStatus() {
		return status;
	}
	
	public String getExpiration() {
		return expiration;
	}
	
	public String getAlgorithm() {
		return algorithm;
	}
	
	public Integer getBitLength() {
		return bitLength;
	}
	
	public String getMode() {
		return mode;
	}
	
	public String getPayload() {
		return payload;
	}
	
	public String getPayloadContentType() {
		return payloadContentType;
	}
	
	public String getPayloadContentEncoding() {
		return payloadContentEncoding;
	}
	
	public void setRef(String ref) {
		this.ref = ref;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void setType(String type) {
		this.type = type;
	}
	
	public void setCreated(String created) {
		this.created = created;
	}
	
	public void setStatus(String status) {
		this.status = status;
	}
	
	public void setExpiration(String expiration) {
		this.expiration = expiration;
	}
	
	public void setAlgorithm(String algorithm) {
		this.algorithm = algorithm;
	}
	
	public void setBitLength(Integer bitLength) {
		this.bitLength = bitLength;
	}
	
	public void setMode(String mode) {
		this.mode = mode;
	}
	
	public void setPayload(String payload) {
		this.payload = payload;
	}
	
	public void setPayloadContentType(String payloadContentType) {
		this.payloadContentType = payloadContentType;
	}
	
	public void setPayloadContentEncoding(String payloadContentEncoding) {
		this.payloadContentEncoding = payloadContentEncoding;
	}
	
	@Override
    public String toString() {
		String str = "name: " + name + "\r\n" +
            "secret_ref: " + ref + "\r\n" + 
            "secret_type: " + type + "\r\n" +
            "created: " + created + "\r\n" +
            "updated: " + updated + "\r\n" +
            "status: " + status + "\r\n" +
            "expiration: " + expiration + "\r\n" +
            "algorithm: " + algorithm + "\r\n" +
            "bit_length: " + bitLength + "\r\n" +
            "mode: " + mode + "\r\n" +
            "payload: " + payload + "\r\n" +
            "payload_type: " + payloadContentType + "\r\n" +
            "payload_encoding: " + payloadContentEncoding + "\r\n" +
            "content_types: " + contentTypes + "\r\n" +
            "creator_id: " + creatorId;
        return str;     
	}
}