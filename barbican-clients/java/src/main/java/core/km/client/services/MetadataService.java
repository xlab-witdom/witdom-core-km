package core.km.client.services;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;

import core.km.client.components.Secret;
import core.km.client.utils.KMException;
import core.km.client.utils.ResponseUtils;

/**
 * Java client for Barbican's metadata service.
 * @author Eduardo Gonz�lez Real <eduardo.gonzalezreal@atos.net>
 */
public class MetadataService {
	
	private static final Logger LOGGER = Logger.getLogger(MetadataService.class);
	
	protected URL endpoint;
	protected String xAuthToken;
	protected String xProjectId;
	protected HttpClient client;
	protected SecretService secretService;

	/**
	 * <code>MetadataService</code> parameterized constructor, initializes the 
	 * HTTP client for the metadata service.
	 * @param endpoint URL for the Barbican server.
	 * @param xAuthToken Authority verification token.
	 * @param xProjectId Project identification
	 * @param secretService Secret service to localize and retrieve secrets.
	 */
	public MetadataService(URL endpoint, String xAuthToken, String xProjectId, SecretService secretService) {
		this.endpoint = endpoint;
		this.xAuthToken = xAuthToken;
		this.xProjectId = xProjectId;
		this.secretService = secretService;
		client = HttpClientBuilder.create().build();
	}
	
	/**
	 * <code>MetadataService</code> parameterized constructor, initializes the 
	 * HTTP client for the metadata service.
	 * @param endpoint URL for the Barbican server.
	 * @param xAuthToken Authority verification token.
	 * @param secretService Secret service to localize and retrieve secrets.
	 */
	public MetadataService(URL endpoint, String xAuthToken, SecretService secretService) {
		this.endpoint = endpoint;
		this.xAuthToken = xAuthToken;
		this.secretService = secretService;
		client = HttpClientBuilder.create().build();
	}
	
	/**
	 * Lists a secret�s user-defined metadata.
	 * @param secretName Name of secret.
	 * @return A list with the metadata that was found.
	 * @throws KMException Throwed because of an unsuccessful response from the server, 
	 * an error in the HTTP protocol or a failed or interrupted I/O operation.
	 */
	public Map<String, String> list(String secretName) throws KMException {
		Secret secret = secretService.get(secretName);
		
		String[] secretRefSplitted = secret.getRef().split("/"); // For Windows only
		String ref = endpoint.toString() + secretRefSplitted[secretRefSplitted.length-1] + "/metadata"; // Splitted is for Windows only, use secretEntry.getRef() in Linux
		HttpGet request = new HttpGet(ref); 
		request.addHeader("content-type", "application/json");
		request.addHeader("X-Auth-Token", xAuthToken);
		if(xProjectId != null) {
			request.addHeader("X-Project-Id", xProjectId);
		}

		try(CloseableHttpResponse response = (CloseableHttpResponse) client.execute(request)) {
			int responseCode = response.getStatusLine().getStatusCode();
			if(responseCode != 200) {
				throw new KMException(responseCode, ResponseUtils.bodyToString(response.getEntity().getContent()));
			}
			LOGGER.debug("GET request to URL : " + ref + " | Response: " + responseCode);

			BufferedReader br = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			String responseContent = br.readLine();
			br.close();
			
			JsonNode jsonObj = new ObjectMapper().readTree(responseContent);
			
			Map<String, String> jsonMap = new HashMap<String, String>();
			if(jsonObj.size() != 0) {
				jsonMap = new ObjectMapper().readValue(jsonObj.get("metadata"), new TypeReference<Map<String, String>>(){});
			}
			
			return jsonMap;
		} catch(ClientProtocolException e) {
			throw new KMException(e.getMessage(), e);
		} catch (IOException e) {
			throw new KMException(e.getMessage(), e);
		}
	}
	
	/**
	 * Retrieves a concrete metadata given the name of the secret and the metadata key.
	 * @param secretName Name of secret.
	 * @param key Key of the Metadata key/value pair.
	 * @return A String with the metadata value.
	 * @throws KMException Throwed because of an unsuccessful response from the server, 
	 * an error in the HTTP protocol or a failed or interrupted I/O operation.
	 */
	public String get(String secretName, String key) throws KMException {
		Map<String, String> metadata = list(secretName);
		if(!metadata.isEmpty()) {
			return metadata.get(key);
		} else {
			return null;
		}
	}
	
	/**
	 * Sets the metadata for a secret. Any metadata that was previously set 
	 * will be deleted and replaced.
	 * @param secretName Name of secret.
	 * @param metadata Metadata to be stored.
	 * @throws KMException Throwed because of an unsuccessful response from the server, 
	 * a failed or interrupted I/O operation or due to problems generating JSON.
	 */
	public void store(String secretName, Map<String, String> metadata) throws KMException {
		Secret secret = secretService.get(secretName);
		
		String[] secretRefSplitted = secret.getRef().split("/"); // For Windows only
		String ref = endpoint.toString() + secretRefSplitted[secretRefSplitted.length-1] + "/metadata"; // Splitted is for Windows only, use secretEntry.getRef() in Linux
		HttpPut request = new HttpPut(ref); 
		request.addHeader("content-type", "application/json");
		request.addHeader("X-Auth-Token", xAuthToken);
		if(xProjectId != null) {
			request.addHeader("X-Project-Id", xProjectId);
		}
		
		try {
			String jsonToStore = "{\"metadata\":" + new ObjectMapper().writeValueAsString(metadata) + "}";
			StringEntity jsonEntity = new StringEntity(jsonToStore);
			request.setEntity(jsonEntity);
		} catch(JsonMappingException e) {
			throw new KMException(e.getMessage(), e);
		} catch (JsonGenerationException e) {
			throw new KMException(e.getMessage(), e);
		} catch (IOException e) {
			throw new KMException(e.getMessage(), e);
		}
		
		try(CloseableHttpResponse response = (CloseableHttpResponse) client.execute(request)) {
			int responseCode = response.getStatusLine().getStatusCode();
			if(responseCode != 201) {
				throw new KMException(responseCode, ResponseUtils.bodyToString(response.getEntity().getContent()));
			}
			LOGGER.debug("PUT request to URL : " + ref + " | Response: " + responseCode);
		} catch(JsonMappingException e) {
			throw new KMException(e.getMessage(), e);
		} catch (JsonGenerationException e) {
			throw new KMException(e.getMessage(), e);
		} catch (IOException e) {
			throw new KMException(e.getMessage(), e);
		}
	}
	
	/**
	 * Adds a new key/value pair to the secret�s user metadata. The key sent 
	 * in the request must not already exist in the metadata. The key must 
	 * also be in lowercase, otherwise it will automatically be changed to 
	 * lowercase.
	 * @param secretName Name of secret.
	 * @param key Key of the new key/value pair.
	 * @param value Value of the new key/value pair.
	 * @throws KMException Throwed because of an unsuccessful response from the server, 
	 * a failed or interrupted I/O operation or due to problems generating JSON.
	 */
	public void add(String secretName, String key, String value) throws KMException {
		Secret secret = secretService.get(secretName);
		
		String[] secretRefSplitted = secret.getRef().split("/"); // For Windows only
		String ref = endpoint.toString() + secretRefSplitted[secretRefSplitted.length-1] + "/metadata"; // Splitted is for Windows only, use secretEntry.getRef() in Linux
		HttpPost request = new HttpPost(ref); 
		request.addHeader("content-type", "application/json");
		request.addHeader("X-Auth-Token", xAuthToken);
		if(xProjectId != null) {
			request.addHeader("X-Project-Id", xProjectId);
		}
		
		Map<String, String> metadata = new HashMap<String, String>();
		metadata.put("key", key);
		metadata.put("value", value);
		
		try {
			String jsonToStore = new ObjectMapper().writeValueAsString(metadata);
			StringEntity jsonEntity = new StringEntity(jsonToStore);
			request.setEntity(jsonEntity);
		} catch (IOException e) {
			throw new KMException(e.getMessage(), e);
		}
		
		try(CloseableHttpResponse response = (CloseableHttpResponse) client.execute(request)) {
			int responseCode = response.getStatusLine().getStatusCode();
			if(responseCode != 201) {
				throw new KMException(responseCode, ResponseUtils.bodyToString(response.getEntity().getContent()));
			}
			LOGGER.debug("POST request to URL : " + ref + " | Response: " + responseCode);
		} catch(JsonMappingException e) {
			throw new KMException(e.getMessage(), e);
		} catch (JsonGenerationException e) {
			throw new KMException(e.getMessage(), e);
		} catch (IOException e) {
			throw new KMException(e.getMessage(), e);
		}
	}
	
	/**
	 * Updates an existing key/value pair in the secret�s user metadata. The key sent 
	 * in the request must already exist in the metadata. The key must also be in lowercase, 
	 * otherwise it will automatically be changed to lowercase.
	 * @param secretName Name of secret.
	 * @param key Key of the key/value pair.
	 * @param value New value of the key/value pair.
	 * @throws KMException Throwed because of an unsuccessful response from the server, 
	 * a failed or interrupted I/O operation or due to problems generating JSON.
	 */
	public void update(String secretName, String key, String value) throws KMException {
		Secret secret = secretService.get(secretName);
		
		String keyFormatted = key.replaceAll(" +", "%20");
		String[] secretRefSplitted = secret.getRef().split("/"); // For Windows only
		String ref = endpoint.toString() + secretRefSplitted[secretRefSplitted.length-1] + "/metadata/" + keyFormatted; // Splitted is for Windows only, use secretEntry.getRef() in Linux
		HttpPut request = new HttpPut(ref); 
		request.addHeader("content-type", "application/json");
		request.addHeader("X-Auth-Token", xAuthToken);
		if(xProjectId != null) {
			request.addHeader("X-Project-Id", xProjectId);
		}
		
		Map<String, String> metadata = new HashMap<String, String>();
		metadata.put("value", value);
		metadata.put("key", key);
		
		try {
			String jsonToStore = new ObjectMapper().writeValueAsString(metadata);
			StringEntity jsonEntity = new StringEntity(jsonToStore);
			request.setEntity(jsonEntity);
		} catch (IOException e) {
			throw new KMException(e.getMessage(), e);
		}
		
		try(CloseableHttpResponse response = (CloseableHttpResponse) client.execute(request)) {
			int responseCode = response.getStatusLine().getStatusCode();
			if(responseCode != 200) {
				throw new KMException(responseCode, ResponseUtils.bodyToString(response.getEntity().getContent()));
			}
			LOGGER.debug("PUT request to URL : " + ref + " | Response: " + responseCode);
		} catch(JsonMappingException e) {
			throw new KMException(e.getMessage(), e);
		} catch (JsonGenerationException e) {
			throw new KMException(e.getMessage(), e);
		} catch (IOException e) {
			throw new KMException(e.getMessage(), e);
		}
	}
	
	/**
	 * Deletes secret metadata given the secret name and the metadata key.
	 * @param secretName Name of secret.
	 * @param key Key of metadata.
	 * @throws KMException Throwed because of an unsuccessful response from the server, 
	 * an error in the HTTP protocol or a failed or interrupted I/O operation.
	 */
	public void delete(String secretName, String key) throws KMException {
		Secret secret = secretService.get(secretName);
		
		String keyFormatted = key.replaceAll(" +", "%20");
		String[] secretRefSplitted = secret.getRef().split("/"); // For Windows only
		String ref = endpoint.toString() + secretRefSplitted[secretRefSplitted.length-1] + "/metadata/" + keyFormatted; // Splitted is for Windows only, use secretEntry.getRef() in Linux
		HttpDelete request = new HttpDelete(ref); 
		request.addHeader("X-Auth-Token", xAuthToken);
		if(xProjectId != null) {
			request.addHeader("X-Project-Id", xProjectId);
		}
		
		try(CloseableHttpResponse response = (CloseableHttpResponse) client.execute(request)) {
			int responseCode = response.getStatusLine().getStatusCode();
			if(responseCode != 204) {
				throw new KMException(responseCode, ResponseUtils.bodyToString(response.getEntity().getContent()));
			}
			LOGGER.debug("DELETE request to URL : " + ref + " | Response: " + responseCode);
		} catch(ClientProtocolException e) {
			throw new KMException(e.getMessage(), e);
		} catch (IOException e) {
			throw new KMException(e.getMessage(), e);
		}
	}
}
