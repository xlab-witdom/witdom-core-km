package core.km.client.components;

import java.util.Map;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
public class AccessControlList {
	
	@JsonProperty("acl_ref")
	protected String ref;
	@JsonProperty("project-access")
	protected String projectAccess;
	@JsonProperty("created")
	protected String created;
	@JsonProperty("updated")
	protected String updated;
	@JsonProperty("users")
	protected Map<String, String> users;
	
	public String getRef() {
		return ref;
	}
	
	public String getProjectAccess() {
		return projectAccess;
	}
	
	public String getCreated() {
		return created;
	}
	
	public String getUpdated() {
		return updated;
	}
	
	public Map<String, String> getUsers() {
		return users;
	}
	
	public void setRef(String ref) {
		this.ref = ref;
	}
	
	public void setProjectAccess(String projectAccess) {
		this.projectAccess = projectAccess;
	}
	
	public void setCreated(String created) {
		this.created = created;
	}
	
	public void setUpdated(String updated) {
		this.updated = updated;
	}
	
	public void setUsers(Map<String, String> users) {
		this.users = users;
	}
	
	@Override
    public String toString() {
		String str = "acl_ref: " + ref + "\r\n" +
            "project-access: " + projectAccess + "\r\n" + 
            "created: " + created + "\r\n" +
            "updated: " + updated + "\r\n" +
            "users: " + users;
        return str;     
	}
}
