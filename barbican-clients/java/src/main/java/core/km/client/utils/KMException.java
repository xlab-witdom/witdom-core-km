package core.km.client.utils;

public class KMException extends Exception {

	private static final long serialVersionUID = 1L;
	
	public KMException() {}
	
	public KMException(String message, Throwable e) {
		super(message, e);
	}
	
	public KMException(String message) {
		super(message);
	}
	
	public KMException(Throwable e) {
		super(e);
	}
	
	public KMException(int responseCode, String message) {
		super("Request failed | Response Code: " + responseCode + " | Message: " + message);
	}
	
	public KMException(int responseCode, String message, Throwable e) {
		super("Request failed | Response Code: " + responseCode + " | Message: " + message, e);
	}
}