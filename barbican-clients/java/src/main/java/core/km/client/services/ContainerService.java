package core.km.client.services;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import core.km.client.components.Container;
import core.km.client.utils.HttpDeleteWithBody;
import core.km.client.utils.KMException;
import core.km.client.utils.ResponseUtils;

/**
 * Java client for Barbican's container service.
 * @author Eduardo Gonz�lez Real <eduardo.gonzalezreal@atos.net>
 */

public class ContainerService {

	private static final Logger LOGGER = Logger.getLogger(ContainerService.class);
	
	protected URL endpoint;
	protected String xAuthToken;
	protected String xProjectId;
	protected ACLService aclService;
	protected HttpClient client;
	
	/**
	 * <code>ContainerService</code> parameterized constructor, initializes the 
	 * HTTP client for the container service.
	 * @param endpoint URL for the Barbican server.
	 * @param xAuthToken Authority verification token.
	 * @param xProjectId Project identification.
	 * @throws KMException The endpoint URL is malformed.
	 */
	public ContainerService(URL endpoint, String xAuthToken, String xProjectId) throws KMException {
		try {
			this.endpoint = new URL(endpoint, "containers/");
			this.xAuthToken = xAuthToken;
			this.xProjectId = xProjectId;
			aclService = new ACLService(this.endpoint, this.xAuthToken, this.xProjectId, "containers");
			this.client = HttpClientBuilder.create().build();
		} catch (MalformedURLException e) {
			throw new KMException(e.getMessage(), e);
		}
	}
	
	/**
	 * <code>ContainerService</code> parameterized constructor, initializes the 
	 * HTTP client for the container service.
	 * @param endpoint URL for the Barbican server.
	 * @param xAuthToken Authority verification token.
	 * @throws KMException The endpoint URL is malformed.
	 */
	public ContainerService(URL endpoint, String xAuthToken) throws KMException {
		try {
			this.endpoint = new URL(endpoint, "containers/");
			this.xAuthToken = xAuthToken;
			aclService = new ACLService(this.endpoint, this.xAuthToken, "containers");
			this.client = HttpClientBuilder.create().build();
		} catch (MalformedURLException e) {
			throw new KMException(e.getMessage(), e);
		}
	}
	
	/**
	 * @return HTTP client for the container service.
	 */
	public HttpClient getHttpClient(){
	    return this.client;
	}
	
	/**
	 * Lists the Barbican's containers given an offset and a limit.
	 * @param offset The starting index within the total list of the containers, not required.
	 * @param limit The maximum number of containers to return, not required.
	 * @return A list with the containers that were found.
	 * @throws KMException Throwed because of the endpoint URL is malformed, 
	 * the string could not be parsed as a URI reference, due to a failed or 
	 * interrupted I/O operations or unsuccessful response from the server.
	 */
	public List<Container> list(Integer offset, Integer limit) throws KMException {
		List<Container> containers = null;
		HttpGet request;
		try {
			request = new HttpGet(new URL(endpoint, "?offset=" + offset + "&limit=" + limit).toURI());
			request.addHeader("content-type", "application/json");
			request.addHeader("X-Auth-Token", xAuthToken);
			if(xProjectId != null) {
				request.addHeader("X-Project-Id", xProjectId);
			}
		} catch (MalformedURLException e) {
			throw new KMException(e.getMessage(), e);
		} catch (URISyntaxException e) {
			throw new KMException(e.getMessage(), e);
		}
		
		try(CloseableHttpResponse response = (CloseableHttpResponse) client.execute(request)) {
			int responseCode = response.getStatusLine().getStatusCode();
			if(responseCode != 200) {
				throw new KMException(responseCode, ResponseUtils.bodyToString(response.getEntity().getContent()));
			}
			LOGGER.debug("GET request to URL : " + endpoint + " | Response: " + responseCode);

			BufferedReader br = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			String responseContent = br.readLine();
			br.close();

			JsonNode jsonObj = new ObjectMapper().readTree(responseContent);

			containers = new ArrayList<Container>();
			for(JsonNode jsonEntry : jsonObj.get("containers")) {
				containers.add(new ObjectMapper().treeToValue(jsonEntry, Container.class));
			}
		} catch (IOException e) {
			throw new KMException(e.getMessage(), e);
		}
		
		return containers;
	}
	
	/**
	 * Retrieves a single container given its name.
	 * @param name Name of container.
	 * @return The container if found, null otherwise.
	 * @throws KMException Throwed because of the endpoint URL is malformed, 
	 * the string could not be parsed as a URI reference or due to a failed or 
	 * interrupted I/O operations.
	 */
	public Container get(String name) throws KMException {
		List<Container> containers = list(null, null);
		Container container = null;
		for(Container containerEntry : containers) {
			if(containerEntry.getName().equals(name)) {
				container = containerEntry;
			}
		}
		
		return container;
	}
	
	/**
	 * Creates and stores a container.
	 * @param name Name of container.
	 * @param type Type of container (generic, rsa, or certificate).
	 * @param secretRefs A list of dictionaries containing references to secrets.
	 * @throws KMException Throwed because the string could not be parsed as a 
	 * URI reference, I/O operations failed, due to problems generating JSON or
	 * unsuccessful response from the server.
	 */
	public void create(String name, String type, List<Map<String, String>> secretRefs) throws KMException {
		Container container = new Container();
		container.setName(name);
		if(type != null)
			container.setType(type);
		if(secretRefs != null)
			container.setSecrets(secretRefs);
		create(container);
	}
	
	/**
	 * Stores a container.
	 * @param container Container to be stored.
	 * @throws KMException Throwed because the string could not be parsed as a 
	 * URI reference, I/O operations failed, due to problems generating JSON or
	 * unsuccessful response from the server.
	 */
	public void create(Container container) throws KMException {
		HttpPost request;
		try {
			request = new HttpPost(endpoint.toURI());
			request.addHeader("content-type", "application/json");
			request.addHeader("X-Auth-Token", xAuthToken);
			if(xProjectId != null) {
				request.addHeader("X-Project-Id", xProjectId);
			}
			String jsonInString = new ObjectMapper().writeValueAsString(container); // Convert Object to JSON string
			StringEntity jsonEntity = new StringEntity(jsonInString);
			request.setEntity(jsonEntity);
		} catch(URISyntaxException e) {
			throw new KMException(e.getMessage(), e);
		} catch (JsonGenerationException e) {
			throw new KMException(e.getMessage(), e);
		} catch (JsonMappingException e) {
			throw new KMException(e.getMessage(), e);
		} catch (IOException e) {
			throw new KMException(e.getMessage(), e);
		}
		
		try(CloseableHttpResponse response = (CloseableHttpResponse) client.execute(request)) {
			int responseCode = response.getStatusLine().getStatusCode();
			if(responseCode != 201) {
				throw new KMException(responseCode, ResponseUtils.bodyToString(response.getEntity().getContent()));
			}
			LOGGER.debug("POST request to URL : " + endpoint + " | Response: " + responseCode);
		} catch (IOException e) {
			throw new KMException(e.getMessage(), e);
		}
	}
	
	/**
	 * Deletes a container.
	 * @param name Name of container.
	 * @throws KMException Throwed due to a failed or interrupted I/O operation 
	 * or unsuccessful response from the server.
	 */
	public void delete(String name) throws KMException {
		Container container = get(name);
		
		String[] containerRefSplitted = container.getRef().split("/"); // For Windows only
		String ref = endpoint.toString() + containerRefSplitted[containerRefSplitted.length-1]; // For test only
		HttpDelete request = new HttpDelete(ref); // For Windows only, use secretEntry.getRef() in Linux
		request.addHeader("content-type", "application/json");
		request.addHeader("X-Auth-Token", xAuthToken);
		if(xProjectId != null) {
			request.addHeader("X-Project-Id", xProjectId);
		}
	
		try(CloseableHttpResponse response = (CloseableHttpResponse) client.execute(request)) {
			int responseCode = response.getStatusLine().getStatusCode();
			if(responseCode != 204) {
				throw new KMException(responseCode, ResponseUtils.bodyToString(response.getEntity().getContent()));
			}
			LOGGER.debug("DELETE request to URL : " + container.getRef() + " | Response: " + responseCode);
		} catch (IOException e) {
			throw new KMException(e.getMessage(), e);
		}
	}
	
	/**
	 * Adds a secret to a container, this method is only supported in generic containers.
	 * @param containerName Name of container.
	 * @param secretName Name of secret.
	 * @param secretRef Reference of secret.
	 * @throws KMException Throwed because an I/O operation failed, due to 
	 * problems generating JSON or unsuccessful response from the server.
	 */
	public void addSecret(String containerName, String secretName, String secretRef) throws KMException {
		Container container = get(containerName);
		
		String[] containerRefSplitted = container.getRef().split("/"); // For Windows only
		String ref = endpoint.toString() + containerRefSplitted[containerRefSplitted.length-1] + "/secrets"; // For test only
		HttpPost request = new HttpPost(ref);
		request.addHeader("content-type", "application/json");
		request.addHeader("X-Auth-Token", xAuthToken);
		if(xProjectId != null) {
			request.addHeader("X-Project-Id", xProjectId);
		}
		
		Map<String, String> requestContent = new HashMap<String, String>();
		if(secretName != null)
			requestContent.put("name", secretName);
		requestContent.put("secret_ref", secretRef);

		try {
			String jsonInString = new ObjectMapper().writeValueAsString(requestContent);
			StringEntity jsonEntity = new StringEntity(jsonInString);
			request.setEntity(jsonEntity);
		} catch (IOException e) {
			throw new KMException(e.getMessage(), e);
		}

		try(CloseableHttpResponse response = (CloseableHttpResponse) client.execute(request)) {
			int responseCode = response.getStatusLine().getStatusCode();
			if(responseCode != 201) {
				throw new KMException(responseCode, ResponseUtils.bodyToString(response.getEntity().getContent()));
			}
			LOGGER.debug("POST request to URL : " + ref + " | Response: " + responseCode);
		} catch(JsonMappingException e) {
			throw new KMException(e.getMessage(), e);
		} catch (JsonGenerationException e) {
			throw new KMException(e.getMessage(), e);
		} catch (IOException e) {
			throw new KMException(e.getMessage(), e);
		}
	}
	
	/**
	 * Deletes a secret from a container, this method is only supported in generic containers.
	 * @param containerName Name of container.
	 * @param secretName Name of secret.
	 * @param secretRef Reference of secret.
	 * @throws KMException Throwed because an I/O operation failed, due to 
	 * problems generating JSON or unsuccessful response from the server.
	 */
	public void deleteSecret(String containerName, String secretName, String secretRef) throws KMException {
		Container container = get(containerName);
		
		String[] containerRefSplitted = container.getRef().split("/"); // For Windows only
		String ref = endpoint.toString() + containerRefSplitted[containerRefSplitted.length-1] + "/secrets"; // For test only
		HttpDeleteWithBody request = new HttpDeleteWithBody(ref); // For Windows only, use secretEntry.getRef() in Linux
		request.addHeader("content-type", "application/json");
		request.addHeader("X-Auth-Token", xAuthToken);
		if(xProjectId != null) {
			request.addHeader("X-Project-Id", xProjectId);
		}
		
		Map<String, String> requestContent = new HashMap<String, String>();
		if(secretName != null)
			requestContent.put("name", secretName);
		requestContent.put("secret_ref", secretRef);
		
		try {
			String jsonInString = new ObjectMapper().writeValueAsString(requestContent);
			StringEntity jsonEntity = new StringEntity(jsonInString);
			request.setEntity(jsonEntity);
		} catch (IOException e) {
			throw new KMException(e.getMessage(), e);
		}
		
		try(CloseableHttpResponse response = (CloseableHttpResponse) client.execute(request)) {
			int responseCode = response.getStatusLine().getStatusCode();
			if(responseCode != 204) {
				throw new KMException(responseCode, ResponseUtils.bodyToString(response.getEntity().getContent()));
			}
			LOGGER.debug("DELETE request to URL : " + container.getRef() + " | Response: " + responseCode);
		} catch(JsonMappingException e) {
			throw new KMException(e.getMessage(), e);
		} catch (JsonGenerationException e) {
			throw new KMException(e.getMessage(), e);
		} catch (IOException e) {
			throw new KMException(e.getMessage(), e);
		}
	}
	
	/**
	 * @return Container ACL service.
	 */
	public ACLService acl() {
		return aclService;
	}
}