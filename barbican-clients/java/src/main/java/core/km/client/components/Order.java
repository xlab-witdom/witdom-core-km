package core.km.client.components;

import java.util.Map;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
public class Order {

	@JsonProperty("order_ref")
	protected String ref;
	@JsonProperty("type")
	protected String type;
	@JsonProperty("secret_ref")
	protected String secretRef;
	@JsonProperty("created")
	protected String created;
	@JsonProperty("updated")
	protected String updated;
	@JsonProperty("creator_id")
	protected String creatorId;
	@JsonProperty("status")
	protected String status;
	@JsonProperty("sub_status")
	protected String subStatus;
	@JsonProperty("sub_status_message")
	protected String subStatusMessage;
	@JsonProperty("meta")
	protected Map<String, Object> metadata;
	
	public String getRef() {
		return ref;
	}
	
	public String getType() {
		return type;
	}
	
	public String getSecretRef() {
		return secretRef;
	}
	
	public String getCreated() {
		return created;
	}
	
	public String getUpdated() {
		return updated;
	}
	
	public String getCreatorId() {
		return creatorId;
	}
	
	public String getStatus() {
		return status;
	}
	
	public Map<String, Object> getMetadata() {
		return metadata;
	}
	
	public void setRef(String ref) {
		this.ref = ref;
	}
	
	public void setType(String type) {
		this.type = type;
	}
	
	public void setSecretRef(String secretRef) {
		this.secretRef = secretRef;
	}
	
	public void setCreated(String created) {
		this.created = created;
	}
	
	public void setUpdated(String updated) {
		this.updated = updated;
	}
	
	public void setCreatorId(String creatorId) {
		this.creatorId = creatorId;
	}
	
	public void setStatus(String status) {
		this.status = status;
	}
	
	public void setMetadata(Map<String, Object> metadata) {
		this.metadata = metadata;
	}
	
	@Override
    public String toString() {
		String str = "order_ref: " + ref + "\r\n" +
            "type: " + type + "\r\n" + 
            "secret_ref: " + secretRef + "\r\n" +
            "created: " + created + "\r\n" +
            "updated: " + updated + "\r\n" +
            "creator_id: " + creatorId + "\r\n" +
            "status: " + status + "\r\n" +
            "sub_status: " + subStatus + "\r\n" +
            "sub_status_message: " + subStatusMessage + "\r\n" +
            "meta: " + metadata;
        return str;     
	}
}
