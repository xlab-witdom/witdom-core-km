package core.km.client.components;

import java.util.List;
import java.util.Map;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
public class Container {

	@JsonProperty("container_ref")
	protected String ref;
	@JsonProperty("name")
	protected String name;
	@JsonProperty("type")
	protected String type; // Generic or RSA
	@JsonProperty("status")
	protected String status;
	@JsonProperty("secret_refs")
	protected List<Map<String, String>> secrets;
	@JsonProperty("created")
	protected String created;
	@JsonProperty("updated")
	protected String updated;
	@JsonProperty("consumers")
	protected List<Map<String, String>> consumers;
	@JsonProperty("creator_id")
	protected String creatorId;
	
	// Only for RSA type
	@JsonProperty("private_key")
	protected String privateKey;
	@JsonProperty("public_key")
	protected String publicKey;
	@JsonProperty("private_key_passphrase")
	protected String passphraseKey;
	
	public String getRef() {
		return ref;
	}

	public String getName() {
		return name;
	}

	public String getType() {
		return type;
	}

	public String getStatus() {
		return status;
	}

	public List<Map<String, String>> getSecrets() {
		return secrets;
	}

	public String getCreated() {
		return created;
	}

	public String getUpdated() {
		return updated;
	}

	public List<Map<String, String>> getConsumers() {
		return consumers;
	}

	public String getCreatorId() {
		return creatorId;
	}

	public String getPrivateKey() {
		return privateKey;
	}

	public String getPublicKey() {
		return publicKey;
	}

	public String getPassphraseKey() {
		return passphraseKey;
	}

	public void setRef(String ref) {
		this.ref = ref;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setType(String type) {
		this.type = type;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void setSecrets(List<Map<String, String>> secrets) {
		this.secrets = secrets;
	}

	public void setCreated(String created) {
		this.created = created;
	}

	public void setUpdated(String updated) {
		this.updated = updated;
	}

	public void setConsumers(List<Map<String, String>> consumers) {
		this.consumers = consumers;
	}

	public void setCreatorId(String creatorId) {
		this.creatorId = creatorId;
	}

	public void setPrivateKey(String privateKey) {
		this.privateKey = privateKey;
	}

	public void setPublicKey(String publicKey) {
		this.publicKey = publicKey;
	}

	public void setPassphraseKey(String passphraseKey) {
		this.passphraseKey = passphraseKey;
	}

	@Override
    public String toString() {
		String str = "name: " + name + "\r\n" +
            "container_ref: " + ref + "\r\n" + 
            "container_type: " + type + "\r\n" +
            "created: " + created + "\r\n" +
            "updated: " + updated + "\r\n" +
            "status: " + status + "\r\n" +
            "secrets: " + secrets + "\r\n" +
            "consumers: " + consumers + "\r\n" +
            "private_key: " + privateKey + "\r\n" +
            "public_key: " + publicKey + "\r\n" +
            "passphrase_key: " + passphraseKey;
        return str;     
	}
}
