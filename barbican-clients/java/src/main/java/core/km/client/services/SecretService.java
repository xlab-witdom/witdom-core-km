package core.km.client.services;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import core.km.client.components.Secret;
import core.km.client.utils.KMException;
import core.km.client.utils.ResponseUtils;

/**
 * Java client for Barbican's secret service.
 * @author Eduardo Gonz�lez Real <eduardo.gonzalezreal@atos.net>
 */

public class SecretService {

	final static Logger LOGGER = Logger.getLogger(SecretService.class);
	
	protected URL endpoint;
	protected String xAuthToken;
	protected String xProjectId;
	protected MetadataService metadataService;
	protected ACLService aclService;
	protected HttpClient client;

	/**
	 * <code>SecretService</code> parameterized constructor, initializes the 
	 * HTTP client for the secret service.
	 * @param endpoint URL for the Barbican server.
	 * @param xAuthToken Authority verification token.
	 * @param xProjectId Project identification.
	 * @throws KMException The endpoint URL is malformed.
	 */
	public SecretService(URL endpoint, String xAuthToken, String xProjectId) throws KMException {
		try {
			this.endpoint = new URL(endpoint, "secrets/");
			this.xAuthToken = xAuthToken;
			this.xProjectId = xProjectId;
			metadataService = new MetadataService(this.endpoint, this.xAuthToken, this.xProjectId, this);
			aclService = new ACLService(this.endpoint, this.xAuthToken, this.xProjectId, "secrets");
			client = HttpClientBuilder.create().build();
		} catch(MalformedURLException e) {
			throw new KMException(e.getMessage(), e);
		}
	}
	
	/**
	 * <code>SecretService</code> parameterized constructor, initializes the 
	 * HTTP client for the secret service.
	 * @param endpoint URL for the Barbican server.
	 * @param xAuthToken Authority verification token.
	 * @throws KMException The endpoint URL is malformed.
	 */
	public SecretService(URL endpoint, String xAuthToken) throws KMException {
		try {
			this.endpoint = new URL(endpoint, "secrets/");
			this.xAuthToken = xAuthToken;
			metadataService = new MetadataService(this.endpoint, this.xAuthToken, this.xProjectId, this);
			aclService = new ACLService(this.endpoint, this.xAuthToken, "secrets");
			client = HttpClientBuilder.create().build();
		} catch(MalformedURLException e) {
			throw new KMException(e.getMessage(), e);
		}
	}
	
	/**
	 * @return HTTP client for the secret service.
	 */
	public HttpClient getHttpClient(){
	    return this.client;
	}
	
	/**
	 * Lists the Barbican's secrets given an offset, a limit or a name.
	 * @param offset The starting index within the total list of the secrets, not required.
	 * @param limit The maximum number of secrets to return, not required.
	 * @param name Name of secret, not required.
	 * @return A list with the secrets that were found.
	 * @throws KMException Throwed because of the endpoint URL is malformed, 
	 * the string could not be parsed as a URI reference, due to a failed or 
	 * interrupted I/O operations or unsuccessful response from the server.
	 */
	public List<Secret> list(Integer offset, Integer limit, String name) throws KMException {
		HttpGet request;
		try {
			String nameFormatted = name.replaceAll(" +", "%20");
			request = new HttpGet(new URL(endpoint, "?offset=" + offset + "&limit=" + limit + "&name=" + nameFormatted).toURI());
			request.addHeader("content-type", "application/json");
			request.addHeader("X-Auth-Token", xAuthToken);
			if(xProjectId != null) {
				request.addHeader("X-Project-Id", xProjectId);
			}
		} catch(URISyntaxException e) {
			throw new KMException(e.getMessage(), e);
		} catch (MalformedURLException e) {
			throw new KMException(e.getMessage(), e);
		}
		
		try(CloseableHttpResponse response = (CloseableHttpResponse) client.execute(request)) {
			int responseCode = response.getStatusLine().getStatusCode();
			if(responseCode != 200) {
				throw new KMException(responseCode, ResponseUtils.bodyToString(response.getEntity().getContent()));
			}
			LOGGER.debug("GET request to URL : " + endpoint + " | Response: " + responseCode);

			BufferedReader br = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			String responseContent = br.readLine();
			br.close();

			JsonNode jsonObj = new ObjectMapper().readTree(responseContent);

			List<Secret> secrets = new ArrayList<Secret>();
			for(JsonNode jsonEntry : jsonObj.get("secrets")) {
				secrets.add(new ObjectMapper().treeToValue(jsonEntry, Secret.class));
			}

			return secrets;
		} catch (MalformedURLException e) {
			throw new KMException(e.getMessage(), e);
		} catch (ClientProtocolException e) {
			throw new KMException(e.getMessage(), e);
		} catch (IOException e) {
			throw new KMException(e.getMessage(), e);
		}
	}

	/**
	 * Retrieves a single secret given its name.
	 * @param name Name of secret.
	 * @return The secret if found, null otherwise.
	 * @throws KMException Throwed because of the endpoint URL is malformed, 
	 * the string could not be parsed as a URI reference, due to a failed or 
	 * interrupted I/O operations or unsuccessful response from the server.
	 */
	public Secret get(String name) throws KMException {
		return list(null, null, name).get(0);
	}

	/**
	 * Creates and stores a secret.
	 * @param name Name of secret, not required.
	 * @param payload The secret�s data to be stored, not required.
	 * @param payloadContentType The media type for the content of 
	 * the payload, required if payload is included.
	 * @throws KMException Throwed because the string could not be parsed as a 
	 * URI reference, I/O operations failed, due to problems generating JSON or
	 * unsuccessful response from the server.
	 */
	public void store(String name, String payload, String payloadContentType) throws KMException {
		Secret secret = new Secret();
		secret.setName(name);
		if(payload != null) 
			secret.setPayload(payload);
		if(payloadContentType != null) 
			secret.setPayloadContentType(payloadContentType);
		store(secret);
	}
	
	/**
	 * Stores a secret.
	 * @param secret Secret to be stored.
	 * @throws KMException Throwed because the string could not be parsed as a 
	 * URI reference, I/O operations failed, due to problems generating JSON or
	 * unsuccessful response from the server.
	 */
	public void store(Secret secret) throws KMException {
		HttpPost request;
		try {
			request = new HttpPost(endpoint.toURI());
			request.addHeader("content-type", "application/json");
			request.addHeader("X-Auth-Token", xAuthToken);
			if(xProjectId != null) {
				request.addHeader("X-Project-Id", xProjectId);
			}

			String jsonInString = new ObjectMapper().writeValueAsString(secret); // Convert Object to JSON string
			StringEntity jsonEntity = new StringEntity(jsonInString);
			request.setEntity(jsonEntity);
		} catch (URISyntaxException e) {
			throw new KMException(e.getMessage(), e);
		} catch (JsonGenerationException e) {
			throw new KMException(e.getMessage(), e);
		} catch (JsonMappingException e) {
			throw new KMException(e.getMessage(), e);
		} catch (IOException e) {
			throw new KMException(e.getMessage(), e);
		}
		
		try(CloseableHttpResponse response = (CloseableHttpResponse) client.execute(request)) {
			int responseCode = response.getStatusLine().getStatusCode();
			if(responseCode != 201) {
				throw new KMException(responseCode, ResponseUtils.bodyToString(response.getEntity().getContent()));
			}
			LOGGER.debug("POST request to URL : " + endpoint + " | Response: " + responseCode);
		} catch (JsonGenerationException e) {
			throw new KMException(e.getMessage(), e);
		} catch (JsonMappingException e) {
			throw new KMException(e.getMessage(), e);
		} catch (IOException e) {
			throw new KMException(e.getMessage(), e);
		}
	}

	/**
	 * Deletes a secret.
	 * @param name Name of secret.
	 * @throws KMException Throwed because of an unsuccessful response from the server, 
	 * an error in the HTTP protocol, a failed or interrupted I/O operation or 
	 * the requested operation is not supported.
	 */
	public void delete(String name) throws KMException {
		Secret secret = get(name);

		String[] secretRefSplitted = secret.getRef().split("/"); // For Windows only
		String ref = endpoint.toString() + secretRefSplitted[secretRefSplitted.length-1]; // For test only
		HttpDelete request = new HttpDelete(ref); // For Windows only, use secretEntry.getRef() in Linux
		request.addHeader("content-type", "application/json");
		request.addHeader("X-Auth-Token", xAuthToken);
		if(xProjectId != null) {
			request.addHeader("X-Project-Id", xProjectId);
		}
		
		try(CloseableHttpResponse response = (CloseableHttpResponse) client.execute(request)) {
			int responseCode = response.getStatusLine().getStatusCode();
			if(responseCode != 204) {
				throw new KMException(responseCode, ResponseUtils.bodyToString(response.getEntity().getContent()));
			}
			LOGGER.debug("DELETE request to URL : " + secret.getRef() + " | Response: " + responseCode);
		} catch(ClientProtocolException e) {
			throw new KMException(e.getMessage(), e);
		} catch (UnsupportedOperationException e) {
			throw new KMException(e.getMessage(), e);
		} catch (IOException e) {
			throw new KMException(e.getMessage(), e);
		}
	}
	
	/**
	 * Adds the payload to an existing metadata-only secret. This action can only be 
	 * done for a secret that doesn�t have a payload.
	 * @param name Name of secret.
	 * @param payload Secret payload.
	 * @param contentType Attribute of a normal secret creation request.
	 * @param contentEncoding Attribute of a normal secret creation request, not required.
	 * @throws KMException Throwed because of an unsuccessful response from the server, 
	 * an error in the HTTP protocol, a failed or interrupted I/O operation or 
	 * the character encoding is not supported.
	 */
	public void storePayload(String name, String payload, String contentType, String contentEncoding) throws KMException {
		Secret secret = get(name);
		
		String[] secretRefSplitted = secret.getRef().split("/"); // For Windows only
		String ref = endpoint.toString() + secretRefSplitted[secretRefSplitted.length-1]; // Splitted is for Windows only, use secretEntry.getRef() in Linux
		HttpPut request = new HttpPut(ref); 
		request.addHeader("content-type", contentType);
		request.addHeader("X-Auth-Token", xAuthToken);
		if(xProjectId != null) {
			request.addHeader("X-Project-Id", xProjectId);
		}
		if(contentEncoding != null) {
			request.addHeader("Content-Encoding", contentEncoding);
		}
		
		try {
			StringEntity entity = new StringEntity(payload);
			request.setEntity(entity);
		} catch (UnsupportedEncodingException e) {
			throw new KMException(e.getMessage(), e);
		}
		
		try(CloseableHttpResponse response = (CloseableHttpResponse) client.execute(request)) {
			int responseCode = response.getStatusLine().getStatusCode();
			if(responseCode != 204) {
				throw new KMException(responseCode, ResponseUtils.bodyToString(response.getEntity().getContent()));
			}
			LOGGER.debug("PUT request to URL : " + ref + " | Response: " + responseCode);
		} catch(UnsupportedEncodingException e) {
			throw new KMException(e.getMessage(), e);
		} catch (ClientProtocolException e) {
			throw new KMException(e.getMessage(), e);
		} catch (IOException e) {
			throw new KMException(e.getMessage(), e);
		}
	}
	
	/**
	 * Retrieves a secret�s payload.
	 * @param name Name of secret.
	 * @return String with the payload.
	 * @throws KMException Throwed because of an unsuccessful response from the server, 
	 * an error in the HTTP protocol or a failed or interrupted I/O operation.
	 */
	public String getPayload(String name) throws KMException {
		Secret secret = get(name);
		String[] secretRefSplitted = secret.getRef().split("/"); // For Windows only
		String ref = endpoint.toString() + secretRefSplitted[secretRefSplitted.length-1] + "/payload"; // Splitted is for Windows only, use secretEntry.getRef() in Linux
		HttpGet request = new HttpGet(ref); 
		request.addHeader("Accept", "text/plain");
		request.addHeader("X-Auth-Token", xAuthToken);
		if(xProjectId != null) {
			request.addHeader("X-Project-Id", xProjectId);
		}
		
		try(CloseableHttpResponse response = (CloseableHttpResponse) client.execute(request)) {
			int responseCode = response.getStatusLine().getStatusCode();
			if(responseCode != 200) {
				throw new KMException(responseCode, ResponseUtils.bodyToString(response.getEntity().getContent()));
			}
			LOGGER.debug("GET request to URL : " + ref + " | Response: " + responseCode);
			
			return ResponseUtils.bodyToString(response.getEntity().getContent());
		} catch(ClientProtocolException e) {
			throw new KMException(e.getMessage(), e);
		} catch (IOException e) {
			throw new KMException(e.getMessage(), e);
		}
	}
	
	/**
	 * @return Metadata service.
	 */
	public MetadataService metadata() {
		return metadataService;
	}
	
	/**
	 * @return ACL service.
	 */
	public ACLService acl() {
		return aclService;
	}
}