package core.km.client.services;

import java.net.URL;

import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.log4j.Logger;

import core.km.client.components.AccessControlList;

/**
 * Java client for Barbican's ACL service.
 * @author Eduardo Gonz�lez Real <eduardo.gonzalezreal@atos.net>
 */

public class ACLService {
	
	final static Logger LOGGER = Logger.getLogger(ACLService.class);
	
	protected URL endpoint;
	protected String xAuthToken;
	protected String xProjectId;
	protected String serviceName;
	protected SecretService secretService;
	protected ContainerService containerService;
	protected HttpClient client;

	/**
	 * <code>ACLService</code> parameterized constructor, initializes the 
	 * HTTP client for the secret's ACL service.
	 * @param endpoint URL for the Barbican server.
	 * @param xAuthToken Authority verification token.
	 * @param xProjectId Project identification
	 * @param serviceName URL path name of Barbican's service.
	 */
	public ACLService(URL endpoint, String xAuthToken, String xProjectId, String serviceName) {
		this.endpoint = endpoint;
		this.xAuthToken = xAuthToken;
		this.xProjectId = xProjectId;
		this.serviceName = serviceName;
		client = HttpClientBuilder.create().build();
	}
	
	/**
	 * <code>ACLService</code> parameterized constructor, initializes the 
	 * HTTP client for the secret's ACL service.
	 * @param endpoint URL for the Barbican server.
	 * @param xAuthToken Authority verification token.
	 * @param serviceName URL path name of Barbican's service.
	 */
	public ACLService(URL endpoint, String xAuthToken, String serviceName) {
		this.endpoint = endpoint;
		this.xAuthToken = xAuthToken;
		this.serviceName = serviceName;
		client = HttpClientBuilder.create().build();
	}
	
	public AccessControlList get() {
		return null;
	}
	
	public void store() {
		
	}
	
	public void update() {
		
	}
	
	public void delete() {
		
	}
	
	/**
	 * @return HTTP client for the ACL service.
	 */
	public HttpClient getHttpClient(){
	    return this.client;
	}
}