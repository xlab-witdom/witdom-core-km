package core.km.client.services;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import core.km.client.components.Order;
import core.km.client.utils.KMException;
import core.km.client.utils.ResponseUtils;

/**
 * Java client for Barbican's order service.
 * @author Eduardo Gonz�lez Real <eduardo.gonzalezreal@atos.net>
 */

public class OrderService {
	
	private static final Logger LOGGER = Logger.getLogger(OrderService.class);
	
	protected URL endpoint;
	protected String xAuthToken;
	protected String xProjectId;
	protected HttpClient client;
	
	/**
	 * <code>OrderService</code> parameterized constructor, initializes the 
	 * HTTP client for the order service.
	 * @param endpoint URL for the Barbican server.
	 * @param xAuthToken Authority verification token.
	 * @param xProjectId Project identification.
	 * @throws KMException The endpoint URL is malformed.
	 */
	public OrderService(URL endpoint, String xAuthToken, String xProjectId) throws KMException {
		try {
			this.endpoint = new URL(endpoint, "orders/");
			this.xAuthToken = xAuthToken;
			this.xProjectId = xProjectId;
			client = HttpClientBuilder.create().build();
		} catch (MalformedURLException e) {
			throw new KMException(e.getMessage(), e);
		}
	}
	
	/**
	 * <code>OrderService</code> parameterized constructor, initializes the 
	 * HTTP client for the order service.
	 * @param endpoint URL for the Barbican server.
	 * @param xAuthToken Authority verification token.
	 * @throws KMException The endpoint URL is malformed.
	 */
	public OrderService(URL endpoint, String xAuthToken) throws KMException {
		try {
			this.endpoint = new URL(endpoint, "orders/");
			this.xAuthToken = xAuthToken;
			client = HttpClientBuilder.create().build();
		} catch (MalformedURLException e) {
			throw new KMException(e.getMessage(), e);
		}
	}
	
	/**
	 * Lists a project�s orders given an offset and a limit.
	 * @param offset The starting index within the total list of the orders, not required.
	 * @param limit The maximum number of orders to return, not required.
	 * @return A list with the orders that were found.
	 * @throws KMException Throwed because of the endpoint URL is malformed, 
	 * the string could not be parsed as a URI reference, due to a failed or 
	 * interrupted I/O operations or unsuccessful response from the server.
	 */
	public List<Order> list(Integer offset, Integer limit) throws KMException {
		HttpGet request;
		try {
			request = new HttpGet(new URL(endpoint, "?offset=" + offset + "&limit=" + limit).toURI());
			request.addHeader("content-type", "application/json");
			request.addHeader("X-Auth-Token", xAuthToken);
			if(xProjectId != null) {
				request.addHeader("X-Project-Id", xProjectId);
			}
		} catch(URISyntaxException e) {
			throw new KMException(e.getMessage(), e);
		} catch (MalformedURLException e) {
			throw new KMException(e.getMessage(), e);
		}
		
		try(CloseableHttpResponse response = (CloseableHttpResponse) client.execute(request)) {
			int responseCode = response.getStatusLine().getStatusCode();
			if(responseCode != 200) {
				throw new KMException(responseCode, ResponseUtils.bodyToString(response.getEntity().getContent()));
			}
			LOGGER.debug("GET request to URL : " + endpoint + " | Response: " + responseCode);

			BufferedReader br = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			String responseContent = br.readLine();
			br.close();

			JsonNode jsonObj = new ObjectMapper().readTree(responseContent);

			List<Order> orders = new ArrayList<Order>();
			for(JsonNode jsonEntry : jsonObj.get("orders")) {
				orders.add(new ObjectMapper().treeToValue(jsonEntry, Order.class));
			}
			
			return orders;
		} catch (IOException e) {
			throw new KMException(e.getMessage(), e);
		}
	}
	
	/**
	 * Creates and stores an order.
	 * @param keyType The type of key to be generated. Valid types are key, asymmetric, and certificate.
	 * @param metadata Dictionary containing the secret metadata used to generate the secret.
	 * @throws KMException Throwed because the string could not be parsed as a 
	 * URI reference, I/O operations failed, due to problems generating JSON or
	 * unsuccessful response from the server.
	 */
	public void create(String keyType, Map<String, Object> metadata) throws KMException {
		Order order = new Order();
		order.setType(keyType);
		if(metadata != null)
			order.setMetadata(metadata);
		create(order);
	}
	
	/**
	 * Stores an order.
	 * @param order Order to be stored.
	 * @throws KMException Throwed because the string could not be parsed as a 
	 * URI reference, I/O operations failed, due to problems generating JSON or
	 * unsuccessful response from the server.
	 */
	public void create(Order order) throws KMException {
		HttpPost request;
		try{
			request = new HttpPost(endpoint.toURI());
			request.addHeader("content-type", "application/json");
			request.addHeader("X-Auth-Token", xAuthToken);
			if(xProjectId != null) {
				request.addHeader("X-Project-Id", xProjectId);
			}

			String jsonInString = new ObjectMapper().writeValueAsString(order); // Convert Object to JSON string
			StringEntity jsonEntity = new StringEntity(jsonInString);
			request.setEntity(jsonEntity);
		} catch(URISyntaxException e) {
			throw new KMException(e.getMessage(), e);
		} catch (JsonGenerationException e) {
			throw new KMException(e.getMessage(), e);
		} catch (JsonMappingException e) {
			throw new KMException(e.getMessage(), e);
		} catch (IOException e) {
			throw new KMException(e.getMessage(), e);
		}
		
		try(CloseableHttpResponse response = (CloseableHttpResponse) client.execute(request)) {
			int responseCode = response.getStatusLine().getStatusCode();
			if(responseCode != 202) {
				throw new KMException(responseCode, ResponseUtils.bodyToString(response.getEntity().getContent()));
			}
			LOGGER.debug("POST request to URL : " + endpoint + " | Response: " + responseCode);
		} catch (JsonGenerationException e) {
			throw new KMException(e.getMessage(), e);
		} catch (JsonMappingException e) {
			throw new KMException(e.getMessage(), e);
		} catch (IOException e) {
			throw new KMException(e.getMessage(), e);
		}
	}
	
	/**
	 * Deletes a concrete order.
	 * @param secretName Name of the order's secret
	 * @throws KMException Throwed because of an unsuccessful response from the server, 
	 * an error in the HTTP protocol, a failed or interrupted I/O operation or 
	 * the requested operation is not supported.
	 */
	public void delete(String secretName) throws KMException {
		List<Order> orders = list(null, null);
		Order order = null;
		for(Order orderEntry : orders) {
			if(orderEntry.getMetadata().get("name") == secretName) {
				order = orderEntry;
			}
		}
		
		String[] secretRefSplitted = order.getRef().split("/"); // For Windows only
		String ref = endpoint.toString() + secretRefSplitted[secretRefSplitted.length-1]; // For test only
		HttpDelete request = new HttpDelete(ref); // For Windows only, use orderEntry.getRef() in Linux
		request.addHeader("content-type", "application/json");
		request.addHeader("X-Auth-Token", xAuthToken);
		if(xProjectId != null) {
			request.addHeader("X-Project-Id", xProjectId);
		}
		
		try(CloseableHttpResponse response = (CloseableHttpResponse) client.execute(request)) {
			int responseCode = response.getStatusLine().getStatusCode();
			if(responseCode != 204) {
				throw new KMException(responseCode, ResponseUtils.bodyToString(response.getEntity().getContent()));
			}
			LOGGER.debug("DELETE request to URL : " + order.getRef() + " | Response: " + responseCode);
		} catch(ClientProtocolException e) {
			throw new KMException(e.getMessage(), e);
		} catch (UnsupportedOperationException e) {
			throw new KMException(e.getMessage(), e);
		} catch (IOException e) {
			throw new KMException(e.getMessage(), e);
		}
	}
	
	/**
	 * Gets a concrete order.
	 * @param secretName Name of the order's secret
	 * @return The order object
	 * @throws KMException Throwed because of the endpoint URL is malformed, 
	 * the string could not be parsed as a URI reference, due to a failed or 
	 * interrupted I/O operations or unsuccessful response from the server.
	 */
	public Order get(String secretName) throws KMException {
		List<Order> orders = list(null, null);
		Order order = null;
		for(Order orderEntry : orders) {
			if(orderEntry.getMetadata().get("name") == secretName) {
				order = orderEntry;
			}
		}
		
		return order;
	}
}