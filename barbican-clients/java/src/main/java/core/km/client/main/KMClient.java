package core.km.client.main;

import java.net.URL;

import core.km.client.services.*;
import core.km.client.utils.KMException;

/**
 * Java client for Barbican's services.
 * @author Eduardo Gonz�lez Real <eduardo.gonzalezreal@atos.net>
 */

public class KMClient {
	
	protected SecretService secretService;
	protected ContainerService containerService;
	protected OrderService orderService;
	
	/**
	 * <code>KMClient</code> parameterized constructor, 
	 * initializes the following Barbican's clients:
	 * <ul>
	 * 	<li>Secret Service.</li>
	 * 	<li>Container Service.</li>
	 * 	<li>Order Service.</li>
	 * 	<li>ACL Service.</li>
	 * </ul>
	 * @param endpoint URL for the Barbican server.
	 * @param xAuthToken Authority verification token.
	 * @param xProjectId Project identification.
	 * @throws KMException The endpoint URL is malformed.
	 */
	public KMClient(URL endpoint, String xAuthToken, String xProjectId) throws KMException {
		secretService = new SecretService(endpoint, xAuthToken, xProjectId);
		containerService = new ContainerService(endpoint, xAuthToken, xProjectId);
		orderService = new OrderService(endpoint, xAuthToken, xProjectId);
	}
	
	/**
	 * <code>KMClient</code> parameterized constructor, 
	 * initializes the following Barbican's clients:
	 * <ul>
	 * 	<li>Secret Service.</li>
	 * 	<li>Container Service.</li>
	 * 	<li>Order Service.</li>
	 * 	<li>ACL Service.</li>
	 * </ul>
	 * @param endpoint URL for the Barbican server.
	 * @param xAuthToken Authority verification token.
	 * @throws KMException The endpoint URL is malformed.
	 */
	public KMClient(URL endpoint, String xAuthToken) throws KMException {
		secretService = new SecretService(endpoint, xAuthToken);
		containerService = new ContainerService(endpoint, xAuthToken);
		orderService = new OrderService(endpoint, xAuthToken);
	}
	
	public SecretService secrets() {
		return secretService;
	}
	
	public ContainerService containers() {
		return containerService;
	}
	
	public OrderService orders() {
		return orderService;
	}
}
